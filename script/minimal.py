#!/usr/bin/env python3
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

firefoxOptions = Options()
firefoxOptions.add_argument("--headless")

driver = webdriver.Firefox(options=firefoxOptions)

driver.get("http://getcomposer.org")
# click on download button
driver.find_element_by_css_selector('p.buttons:nth-child(1) > a:nth-child(2)').click()
# get lignes of table
results = driver.find_elements_by_css_selector('#main > table:nth-child(25) > tbody:nth-child(2) > tr')

for result in results:
    print(result.text)

driver.close()