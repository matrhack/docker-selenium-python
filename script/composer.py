#!/usr/bin/env python3
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile

firefoxOptions = Options()
firefoxOptions.add_argument("--headless")

firefoxProfile = FirefoxProfile()
firefoxProfile.set_preference("browser.download.folderList",2)
firefoxProfile.set_preference("browser.download.manager.showWhenStarting", False)
firefoxProfile.set_preference("browser.download.dir","/download/")
firefoxProfile.set_preference("browser.helperApps.neverAsk.saveToDisk", 
    "text/plain, application/octet-stream, application/binary, text/csv, application/csv, application/excel, text/comma-separated-values, text/xml, application/xml")

driver = webdriver.Firefox(options=firefoxOptions, firefox_profile=firefoxProfile)

#open page
driver.get("http://getcomposer.org")
# click on download button
driver.find_element_by_css_selector('p.buttons:nth-child(1) > a:nth-child(2)').click()
# get lignes of table
results = driver.find_elements_by_css_selector('#main > table:nth-child(25) > tbody:nth-child(2) > tr')

i = 0
# iteration
for result in results:
    link = result.find_element_by_css_selector('a')
    print(link.text)
    # go easy on composer website ..
    if i<2:
        print("click")
        # download
        link.click()
    i=i+1

# the end
driver.close()