#!/usr/bin/env python3
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
import time

firefoxOptions = Options()
firefoxOptions.add_argument("--headless")
firefoxOptions.set_capability("deviceName", "iPhone")

firefoxProfile = FirefoxProfile()
firefoxProfile.set_preference("browser.download.folderList",2)
firefoxProfile.set_preference("browser.download.manager.showWhenStarting", False)
firefoxProfile.set_preference("browser.download.dir","/download/")
firefoxProfile.set_preference("browser.helperApps.neverAsk.saveToDisk", 
    "text/plain, application/octet-stream, application/binary, text/csv, application/csv, application/excel, text/comma-separated-values, text/xml, application/xml")

driver = webdriver.Firefox(options=firefoxOptions, firefox_profile=firefoxProfile)

driver.set_window_size(1284, 2778)
#open page
driver.get("https://twitter.com/afpfr")
driver.save_screenshot("/download/test.png")
time.sleep(10)
driver.save_screenshot("/download/test1.png")


# the end
driver.close()